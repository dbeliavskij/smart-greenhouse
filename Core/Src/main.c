/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart2;

/* Definitions for TempHumTask */
osThreadId_t TempHumTaskHandle;
const osThreadAttr_t TempHumTask_attributes = {
  .name = "TempHumTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for LightTask */
osThreadId_t LightTaskHandle;
const osThreadAttr_t LightTask_attributes = {
  .name = "LightTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for UartReportTask */
osThreadId_t UartReportTaskHandle;
const osThreadAttr_t UartReportTask_attributes = {
  .name = "UartReportTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal,
};
/* Definitions for UARTComTask */
osThreadId_t UARTComTaskHandle;
const osThreadAttr_t UARTComTask_attributes = {
  .name = "UARTComTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal,
};
/* Definitions for WateringTask */
osThreadId_t WateringTaskHandle;
const osThreadAttr_t WateringTask_attributes = {
  .name = "WateringTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for WorkTimeTask */
osThreadId_t WorkTimeTaskHandle;
const osThreadAttr_t WorkTimeTask_attributes = {
  .name = "WorkTimeTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityAboveNormal,
};
/* Definitions for UartSem */
osSemaphoreId_t UartSemHandle;
const osSemaphoreAttr_t UartSem_attributes = {
  .name = "UartSem"
};
/* Definitions for SysStatusEvent */
osEventFlagsId_t SysStatusEventHandle;
const osEventFlagsAttr_t SysStatusEvent_attributes = {
  .name = "SysStatusEvent"
};
/* USER CODE BEGIN PV */
uint16_t light_intens = 0;
uint16_t soil_hum = 0;
uint8_t light_low = 25;
uint8_t light_high = 30;
uint8_t soil_hum_high = 50;
uint8_t soil_hum_low = 10;
float temp, hum;
char com[4];
bool fert_pump = true;
uint8_t scheduleDate = 30;
uint8_t worktime_start = 8;
uint8_t worktime_stop = 20;
RTC_TimeTypeDef currTime = {0};
RTC_DateTypeDef currDate = {0};
bool stop_work = false;

typedef struct {
	char *com_rx;
	uint8_t com_num;
} uart_com;

static uart_com lookup_table[] = {
		{"LLS", LLS},
		{"lls", LLS},
		{"HLS", HLS},
		{"hls", HLS},
		{"TME", TME},
		{"tme", TME},
		{"DAT", DAT},
		{"dat", DAT},
		{"SST", SST},
		{"sst", SST},
		{"FPS", FPS},
		{"fps", FPS}
};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM7_Init(void);
static void MX_ADC1_Init(void);
static void MX_RTC_Init(void);
static void MX_ADC2_Init(void);
void StartTempHumTask(void *argument);
void StartLightTask(void *argument);
void StartUartReportTask(void *argument);
void StartUARTComTask(void *argument);
void StartWateringTask(void *argument);
void StartWorkTimeTask(void *argument);

/* USER CODE BEGIN PFP */
void usDelay(uint16_t delay);
bool DHT22_Init(void);
void Pin_Mode_Set(bool mode);
bool DHT22_Acquire(uint8_t data[5]);
bool DHT22_Read(float * temp, float * hum);
void UART_Report_Print(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

/* Configure the peripherals common clocks */
  PeriphCommonClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM7_Init();
  MX_ADC1_Init();
  MX_RTC_Init();
  MX_ADC2_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim7);
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of UartSem */
  UartSemHandle = osSemaphoreNew(1, 1, &UartSem_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of TempHumTask */
  TempHumTaskHandle = osThreadNew(StartTempHumTask, NULL, &TempHumTask_attributes);

  /* creation of LightTask */
  LightTaskHandle = osThreadNew(StartLightTask, NULL, &LightTask_attributes);

  /* creation of UartReportTask */
  UartReportTaskHandle = osThreadNew(StartUartReportTask, NULL, &UartReportTask_attributes);

  /* creation of UARTComTask */
  UARTComTaskHandle = osThreadNew(StartUARTComTask, NULL, &UARTComTask_attributes);

  /* creation of WateringTask */
  WateringTaskHandle = osThreadNew(StartWateringTask, NULL, &WateringTask_attributes);

  /* creation of WorkTimeTask */
  WorkTimeTaskHandle = osThreadNew(StartWorkTimeTask, NULL, &WorkTimeTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the event(s) */
  /* creation of SysStatusEvent */
  SysStatusEventHandle = osEventFlagsNew(&SysStatusEvent_attributes);

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief Peripherals Common Clock Configuration
  * @retval None
  */
void PeriphCommonClock_Config(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the peripherals clock
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc2.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /*RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};*/

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  /*sTime.Hours = 0;
  sTime.Minutes = 0;
  sTime.Seconds = 0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 1;
  sDate.Year = 0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }*/
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 64-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 65535;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD2_Pin PA7 PA8 PA9
                           PA10 */
  GPIO_InitStruct.Pin = LD2_Pin|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void usDelay(uint16_t delay) {
	__HAL_TIM_SET_COUNTER(&htim7, 0);
	while (__HAL_TIM_GET_COUNTER(&htim7) < delay);

}

bool DHT22_Init(void) {
	Pin_Mode_Set(true);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	usDelay(1200);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
	usDelay(30);

	Pin_Mode_Set(false);
	usDelay(40);

	if (!(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9))) {
		usDelay(80);

		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9)) {
			while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9));
			return true;

		}

		else  {
			return false;

		}
	}

	else {
		return false;
	}

}

//if mode is true, GPIO is set as OUTPUT, if false - INPUT
void Pin_Mode_Set(bool mode) {
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pull = GPIO_NOPULL;

	if (mode) {
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;

	}
	else {
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	}

	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

bool DHT22_Acquire(uint8_t data[5]) {
	uint32_t rawdata = 0;
	uint8_t checksum = 0;
	bool checkdata = false;

	for (int8_t i = 31; i >= 0; i--) {
		while(!(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9)));
		usDelay(40);

		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9)) rawdata |= (1UL << i);

		while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9));

	}

	for (int8_t i = 7; i >= 0; i--) {
		while(!(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9)));
		usDelay(40);

		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9)) checksum |= (1UL << i);

		while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9));

	}

	data[0] = (rawdata>>24);
	data[1] = (rawdata>>16) & 0xFF;
	data[2] = (rawdata>>8) & 0xFF;
	data[3] = rawdata & 0xFF;
	data[4] = checksum;

	checkdata = ((data[0] + data[1] + data[2] + data[3]) & 0xFF) == data[4];

	if (checkdata) return true;
	else return false;



}

bool DHT22_Read(float * temp, float * hum) {
	uint8_t data[6];
	uint16_t temp16, hum16;

	if (DHT22_Init()) {
		if (DHT22_Acquire(data)) {
			temp16 = (data[2]<<8) | data[3];
			hum16 = (data[0]<<8) | data[1];
			*temp = temp16 / 10.0f;
			*hum = hum16 / 10.0f;

			return true;

		}

		else {
			osSemaphoreAcquire(UartSemHandle, osWaitForever);
			HAL_UART_Transmit_IT(&huart2, (uint8_t *)"Error during DHT22 data acquire\r\n", 33);
			return false;

		}
	}

	else {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)"Error during DHT22 initialization\r\n", 35);
		return false;

	}
}

void UART_Report_Print(void) {
	char msg[36];

	HAL_RTC_GetTime(&hrtc, &currTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &currDate, RTC_FORMAT_BIN);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "\n\r\n\rReport %d:%d:%d of %d.%d.%d\n\r", currTime.Hours, currTime.Minutes, currTime.Seconds, currDate.Date, currDate.Month, currDate.Year);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Temperature: %.2fC\n\r", temp);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Humidity: %.2f%%\n\r", hum);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Light: %d%%\n\r", light_intens);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Turns on at %d%%\n\r", light_low);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Turns off at %d%%\n\r", light_high);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Soil humidity: %d%%\n\r", soil_hum);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Start watering at %d%%\n\r", soil_hum_low);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Stop watering at %d%%\n\r", soil_hum_high);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	if (fert_pump) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		sprintf(msg, "Water with fertilizer every %d day\n\r", scheduleDate);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	}

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "System starts at %d\n\r", worktime_start);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "System stops at %d\n\r", worktime_stop);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}

uint8_t IntFromCom(char * com) {
	for (int i = 0; i < NKEYS; i++) {
		if (strcmp(com, lookup_table[i].com_rx) == 0) {
			return lookup_table[i].com_num;
		}
	}

	return BAD_COM;
}

void TME_Setup(void) {
	char msg[32];
	char rx_msg[3];
	RTC_TimeTypeDef newTime = {0};

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input hours (from 00 to 23)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 23) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	newTime.Hours = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input minutes (from 00 to 59)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 59) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	newTime.Minutes = atoi(rx_msg);
	newTime.Seconds = 0;

	HAL_RTC_SetTime(&hrtc, &newTime, RTC_FORMAT_BIN);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Time set\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}

void DAT_Setup(void) {
	char msg[31];
	char rx_msg[3];
	RTC_DateTypeDef newDate = {0};

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input day (from 01 to 31)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 1 || atoi(rx_msg) > 31) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	newDate.Date = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input month (from 01 to 12)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 1 || atoi(rx_msg) > 12) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	newDate.Month = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input year (from 00 to 99)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 99) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	newDate.Year = atoi(rx_msg);

	HAL_RTC_SetDate(&hrtc, &newDate, RTC_FORMAT_BIN);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Date set\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}


void LLS_Setup(void) {
	char msg [111];
	char rx_msg[3];

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input value at which auxiliary light will be turned on (from 01 to 99)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 1 || atoi(rx_msg) > 99) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	light_low = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input value at which auxiliary light will be turned off (from 01 to 99 and at least 5 higher than low value)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 1 || atoi(rx_msg) > 99 || (light_low + 5) > atoi(rx_msg)) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	light_high = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Light setup finished\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);
}

void HLS_Setup(void) {
	char msg [107];
	char rx_msg[3];

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input value below which watering will be turned on (from 00 to 99)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 99) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	soil_hum_low = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input value above which watering will be turned off (from 00 to 99 and at least 5 higher than low value)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 99 || (soil_hum_low + 5) > atoi(rx_msg)) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	soil_hum_high = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Watering setup finished\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}

void FPS_Setup(void) {
	char msg [87];
	char rx_msg[3];

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input day at which fertilizer will be used monthly (from 01 to 30, 00 to deactivate)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 30) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	if (atoi(rx_msg) == 0) {
		fert_pump = false;
	}

	else {
		scheduleDate = atoi(rx_msg);
		fert_pump = true;
	}

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Fertilizer setup finished\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}

void SST_Setup(void) {
	char msg [61];
	char rx_msg[3];

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input hour at which system will start work (from 00 to 23)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 23) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	worktime_start = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Input hour at which system will stop work (from 00 to 23)\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
	osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	while (atoi(rx_msg) < 0 || atoi(rx_msg) > 23 || atoi(rx_msg) == worktime_start) {
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
		HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_msg, 2);
		osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
	}

	worktime_stop = atoi(rx_msg);

	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	sprintf(msg, "Work time set\n\r");
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg));
	osSemaphoreAcquire(UartSemHandle, osWaitForever);
	osSemaphoreRelease(UartSemHandle);

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART2) {
		osSemaphoreRelease(UartSemHandle);

	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART2) {

		uint32_t flags = osEventFlagsGet(SysStatusEventHandle);
		osEventFlagsSet(SysStatusEventHandle, DATA_RX_COMPLETE | flags);

	}
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartTempHumTask */
/**
  * @brief  Function implementing the TempHumTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartTempHumTask */
void StartTempHumTask(void *argument)
{
  /* USER CODE BEGIN 5 */
	uint32_t flags;
  /* Infinite loop */
  for(;;)
  {
	osEventFlagsWait(SysStatusEventHandle, MEASURE_READY, osFlagsWaitAll | osFlagsNoClear, osWaitForever);

	if (stop_work) {
		osThreadSuspend(TempHumTaskHandle);

	}

	while (osEventFlagsGet(SysStatusEventHandle) & TEMP_HUM_READY) {
		osDelay(UPDATE_RATE / 2);
	}

	DHT22_Read(&temp, &hum);
	flags = osEventFlagsGet(SysStatusEventHandle);
	osEventFlagsSet(SysStatusEventHandle, flags | TEMP_HUM_READY);
    osDelay(UPDATE_RATE);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartLightTask */
/**
* @brief Function implementing the LightTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartLightTask */
void StartLightTask(void *argument)
{
  /* USER CODE BEGIN StartLightTask */
	uint32_t flags;
	bool light_on = false;
  /* Infinite loop */
  for(;;)
  {
	  osEventFlagsWait(SysStatusEventHandle, MEASURE_READY, osFlagsWaitAll | osFlagsNoClear, osWaitForever);

	  if (stop_work) {
		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
			osThreadSuspend(LightTaskHandle);

	  }

	  while (osEventFlagsGet(SysStatusEventHandle) & LIGHT_READY) {
		  osDelay(UPDATE_RATE / 2);
	  }

	  if (light_on) {
		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
		  osDelay(2000);

	  }

	  HAL_ADC_Start(&hadc1);
	  HAL_ADC_PollForConversion(&hadc1, 1);
	  light_intens = HAL_ADC_GetValue(&hadc1);
	  light_intens = (light_intens * 100) / 4095; //Convert to percentage

	  if (light_intens < light_high && light_on) {
		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

	  }
	  else if (light_intens < light_low && !light_on) {
		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
		  light_on = !light_on;
	  }
	  else if (light_intens > light_high && light_on) {
		  light_on = !light_on;

	  }

	  flags = osEventFlagsGet(SysStatusEventHandle);
	  osEventFlagsSet(SysStatusEventHandle, flags | LIGHT_READY);
	  osDelay(UPDATE_RATE);
  }
  /* USER CODE END StartLightTask */
}

/* USER CODE BEGIN Header_StartUartReportTask */
/**
* @brief Function implementing the UartReportTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartUartReportTask */
void StartUartReportTask(void *argument)
{
  /* USER CODE BEGIN StartUartReportTask */
  uint32_t flags;
  flags = osEventFlagsGet(SysStatusEventHandle);
  osEventFlagsSet(SysStatusEventHandle, flags | DATA_TX_COMPLETE);
  /* Infinite loop */
  for(;;)
  {
	flags = osEventFlagsWait(SysStatusEventHandle, TX_READY, osFlagsWaitAll | osFlagsNoClear, UPDATE_RATE * 2);

	if (stop_work) {
		osThreadSuspend(UartReportTaskHandle);

	}

	osEventFlagsClear(SysStatusEventHandle, DATA_TX_COMPLETE | TEMP_HUM_READY | LIGHT_READY | SOIL_HUM_READY);
	UART_Report_Print();
	flags = osEventFlagsGet(SysStatusEventHandle);
	osEventFlagsSet(SysStatusEventHandle, flags | DATA_TX_COMPLETE);

  }
  /* USER CODE END StartUartReportTask */
}

/* USER CODE BEGIN Header_StartUARTComTask */
/**
* @brief Function implementing the UARTComTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartUARTComTask */
void StartUARTComTask(void *argument)
{
  /* USER CODE BEGIN StartUARTComTask */
  uint32_t flags = osEventFlagsGet(SysStatusEventHandle);
  osEventFlagsSet(SysStatusEventHandle, flags | SETUP_COMPLETE);
  /* Infinite loop */
  for(;;)
  {
	HAL_UART_Receive_IT(&huart2, (uint8_t *)com, 3);
    osEventFlagsWait(SysStatusEventHandle, DATA_RX_COMPLETE, osFlagsWaitAll, osWaitForever);
    osEventFlagsClear(SysStatusEventHandle, SETUP_COMPLETE);

    switch (IntFromCom(com)) {
		case LLS:
			LLS_Setup();
			break;

		case HLS:
			HLS_Setup();
			break;

		case TME:
			TME_Setup();
			break;

		case DAT:
			DAT_Setup();
			break;

		case SST:
			SST_Setup();
			break;

		case FPS:
			FPS_Setup();
			break;

		default:
			HAL_UART_Transmit(&huart2, (uint8_t *)"Unrecognized command!\n\r", 23, 100);
			break;
	}

    flags = osEventFlagsGet(SysStatusEventHandle);
    osEventFlagsSet(SysStatusEventHandle, flags | SETUP_COMPLETE);

  }
  /* USER CODE END StartUARTComTask */
}

/* USER CODE BEGIN Header_StartWateringTask */
/**
* @brief Function implementing the WateringTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartWateringTask */
void StartWateringTask(void *argument)
{
  /* USER CODE BEGIN StartWateringTask */
	uint32_t flags;
	bool water_fert = false;
	bool fert_done = false;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
  /* Infinite loop */
  for(;;)
  {
	  osEventFlagsWait(SysStatusEventHandle, MEASURE_READY, osFlagsWaitAll | osFlagsNoClear, osWaitForever);

	  if (stop_work) {
			osThreadSuspend(WateringTaskHandle);

	  }

      while (osEventFlagsGet(SysStatusEventHandle) & SOIL_HUM_READY) {
    	  osDelay(UPDATE_RATE / 2);
	  }

	  HAL_ADC_Start(&hadc2);
	  HAL_ADC_PollForConversion(&hadc2, 1);
	  soil_hum = HAL_ADC_GetValue(&hadc2);
	  soil_hum = (soil_hum * 100) / 3968; //Convert to percentage
	  flags = osEventFlagsGet(SysStatusEventHandle);
	  osEventFlagsSet(SysStatusEventHandle, flags | SOIL_HUM_READY);
	  if (!fert_pump) {
		  water_fert = false;
		  fert_done = false;
	  }
	  else if (fert_pump && (currDate.Date == scheduleDate) && !fert_done) {
		  water_fert = true;
	  }

	  else if (fert_pump && currDate.Date != scheduleDate && fert_done) {
		  fert_done = false;
		  water_fert = false;
	  }

	  if (soil_hum < soil_hum_low) {
		  if (water_fert) {
			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);

		  }
		  else {
			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
		  }

		  while (soil_hum < soil_hum_high) {
			  osDelay(100);
			  HAL_ADC_Start(&hadc2);
			  HAL_ADC_PollForConversion(&hadc2, 100);
			  soil_hum = HAL_ADC_GetValue(&hadc2);
			  soil_hum = (soil_hum * 100) / 3968;

		  }
	      if (water_fert) {
	    	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
	    	  fert_done = true;

		  }
		  else {
			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
		  }

	  }

	  osDelay(UPDATE_RATE);
  }
  /* USER CODE END StartWateringTask */
}

/* USER CODE BEGIN Header_StartWorkTimeTask */
/**
* @brief Function implementing the WorkTimeTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartWorkTimeTask */
void StartWorkTimeTask(void *argument)
{
  /* USER CODE BEGIN StartWorkTimeTask */
  /* Infinite loop */
  for(;;)
  {
	HAL_RTC_GetDate(&hrtc, &currDate, RTC_FORMAT_BIN);
	HAL_RTC_GetTime(&hrtc, &currTime, RTC_FORMAT_BIN);

	if ((currTime.Hours >= worktime_start && currTime.Hours <= worktime_stop) && stop_work) {
		stop_work = false;
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)"\n\r\n\rSystem starts working\n\r", 27);

		osThreadResume(WateringTaskHandle);
		osThreadResume(TempHumTaskHandle);
		osThreadResume(LightTaskHandle);
		osThreadResume(UartReportTaskHandle);

	}
	else if ((currTime.Hours >= worktime_stop || currTime.Hours < worktime_start) && !stop_work) {
		stop_work = true;
		osSemaphoreAcquire(UartSemHandle, osWaitForever);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)"\n\r\n\rSystems stops working\n\r", 27);

	}
    osDelay(10000);
  }
  /* USER CODE END StartWorkTimeTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
