# Smart greenhouse

**This project is currently in development**

Aim of the project is to develop a system, which would observe plant enviroment and would take action to miantain enviroment parameters within desired boundaries. System will be connected to WiFi and controlled through WEB UI.

## Funkcionalumas

- Air temperature and humidity measurement
- Light intensity measurement
- Auxilary lightning control
- Soil humidity measurement
- Water pump control
- Possibility to schedule watering of fertilizer (monthly)
- System control through WEB UI

## Required parts

- Temperatūros ir drėgmės jutiklis (https://www.anodas.lt/dht11-temperaturos-ir-dregmes-jutiklis-modulis-kabeliai?search=oro%20temperat%C5%ABros%20jutiklis)
- Šviesos intensyvumo jutiklis (https://www.anodas.lt/temt6000-analoginis-sviesos-intensyvumo-jutiklis-sparkfun)
- LED apšvietimas (https://www.anodas.lt/20w-cob-led-230v-augalu-auginimui)
- Dirvožemio drėgmės jutiklis (https://www.anodas.lt/dirvozemio-dregmes-jutiklis-zondas-analogas-iduino-me110?search=dirvo%C5%BEemio%20dr%C4%97gm%C4%97s)
- 2 x vandens pompa (https://www.anodas.lt/rs-360-mini-vandens-pompa-dc-4-12v?search=pompa)
- 2 x vamzdelis (https://www.anodas.lt/pvc-vamzdelis-vandens-pompoms-4x6mm-1m?search=4%20mm%20vamzdelis)
- WiFi modulis (https://www.anodas.lt/esp8266-esp-01-wifi-modulis?sort=p.price&order=ASC)
- Relės (https://www.anodas.lt/4-kanalu-rele-modulis)
- DC/DC įtampos keitiklis (https://www.anodas.lt/dc-dc-itampos-keitiklis-ams1117-6-25-12v-5v-800ma-step-down)
- Maitinimo šaltinis (https://www.anodas.lt/impulsinis-maitinimo-saltinis-12v-3a-36w?sort=p.price&order=ASC)
- Maketavimo laidai (https://www.anodas.lt/maketavimo-laidai-t-m-30cm-40vnt ir https://www.anodas.lt/maketavimo-laidai-t-t-10cm-40vnt)
- Laidas (https://www.anodas.lt/laidas-su-kistuku-3m)


## Development steps

At the beggining system will communicate through UART, later on a WEB UI will be added.

Things to implement:

1. Temperature and humidity readings **(Completed)**
2. UART interface for showing results **(Needs improving)**
3. Light intensity readings (with result reporting) **(Implemented via polling)**
4. Auxiliary light control logic **(Completed)**
5. Light control through UART **(Completed)**
6. Soil humidity measuring (with result reporting) **(Completed)**
7. Pump control (2 pumps) **(Completed)**
8. Operation and stadby time setup **(Completed)**
9. WEB UI